
import {StyleSheet, Dimensions} from "react-native"
import {dimensions, padding, fonts, colors, baseStyles} from "app/src/styles/AppStyle"

export const styles = StyleSheet.create({
    content: {
        flex: 1
    },

    container: {
        // backgroundColor: colors.primary_blue,
        width: dimensions.fullWidth,
        marginTop: padding.big * 2
    },

    headerContainer: {
        paddingTop: padding.medium,
        flexDirection: "row",
        alignItems: "center"
    },

    headerIcon: {
        resizeMode: "contain",
        width: 50,
        height: 50,
        marginLeft: padding.small,
        alignSelf: "flex-start"
    },

    headerText: {
        fontFamily: "vincHand",
        fontSize: fonts.large,
        marginLeft: padding.small,
        alignSelf: "stretch"
    },

    contentText: {
        fontFamily: "vincHand",
        fontSize: fonts.medium,
        marginTop: padding.medium,
        marginLeft: padding.big,
        marginRight: padding.medium,
        alignSelf: "stretch"
    },

    buttonStyle: {
        marginTop: padding.medium,
        marginLeft: padding.big,
        marginBottom: padding.medium,
        alignSelf: "flex-start",
    },
});

// export const styles = [baseStyles, localStyles];
