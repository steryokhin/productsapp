// Product.js

import React, {Component} from "react";
import {View, Text, Image, ScrollView} from "react-native";
import {appStyles} from "app/src/styles/AppStyle";
import {styles} from "./styles";
import {colors} from "app/src/styles/AppStyle";
import BrandSafeAreaView from "app/src/components/common/BrandSafeAreaView"
import Button from "app/src/components/common/Button"

export class Product extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.getParam("name")
        };

    };

    transfer = () => {
        console.log("transferring");
        this.props.navigation.navigate("ProductListScreen");
    };

    render() {
        const { navigation } = this.props;
        const name = navigation.getParam("name");
        const icon = navigation.getParam("icon");
        const sentences = navigation.getParam("sentences");
        return (
            <BrandSafeAreaView>
                <ScrollView style={styles.content}>
                    <View style={styles.container}>
                        <View style={styles.headerContainer}>
                            <Image source={icon} style={styles.headerIcon}/>
                            <Text style={styles.headerText}>{name}</Text>
                        </View>
                        <Text style={styles.contentText}>
                            {sentences}
                        </Text>
                        <Button style={styles.buttonStyle} onPress={() => this.transfer()}>
                            All Products
                        </Button>
                    </View>
                </ScrollView>
            </BrandSafeAreaView>
        )
    }
}

export default Product
