// Login.js

import React, {Component} from "react";
import {View, Text, Image, ActivityIndicator, KeyboardAvoidingView} from "react-native";
import {TextInput} from "react-native-gesture-handler";
import {currentDayOfWeek} from "app/src/utility/Utility";
import {appStyles, padding} from "app/src/styles/AppStyle";
import {styles} from "./styles";
import BrandSafeAreaView from "app/src/components/common/BrandSafeAreaView";
import Button from "app/src/components/common/Button"

let Constants = {
    LOGIN_PATH: "http://ecsc00a02fb3.epam.com/index.php/rest/V1/integration/customer/token",
    LOGIN_CONTENT_TYPE_VALUE: "application/json",
    LOGIN_CONTENT_TYPE_KEY: "Content-Type"
};

export class Login extends Component {
    static navigationOptions = {
        title: "Login",
        header: null
    };

    state = {
        username: "",
        password: "",
        isLoginInProgress: false,
        message: ""
    };

    /// Login event handler
    onLogIn = () => {
        console.log("onLogIn");
        this.props.navigation.navigate("ProductListScreen");
    };

    onButtonTouch = () => {
        console.log("onButtonTouch");

        this.doLogin()
    };

    /// proceed login with sending request and updating ui
    doLogin = () => {
        this.setState({isLoginInProgress: true, message: ""});

        var params = {
            username: this.state.username,
            password: this.state.password
        };

        var isLoggedIn = false;
        fetch(Constants.LOGIN_PATH, {
                method: "POST",
                headers: {
                    "Content-Type": Constants.LOGIN_CONTENT_TYPE_VALUE
                },
                body: JSON.stringify({username: params.username, password: params.password})
            })
            .then((response) => {
                const data = response.json();
                const statusCode = response.status;

                return Promise.all([statusCode, data])
            })
            .then(([statusCode, data]) => {
                console.log("response code: " + statusCode);
                console.log("response data: " + data);
                if (statusCode == 200) {
                    console.log("LoggedIn Successfully");
                    isLoggedIn = true;
                } else {
                    this.setState({message: data.message})
                }
            })
            .then(() => {
                this.setState({isLoginInProgress: false});
                if (isLoggedIn) {
                    this.onLogIn()
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({message: err.message});
                this.setState({isLoginInProgress: false})
            })

    };

    render() {
        return (
            <BrandSafeAreaView>
                <View style={styles.headerContainer}>
                    <View style={styles.headerImageStyle}>
                        <Image source={require("../../assets/pikaguitar.gif")} style={styles.imageFit} />
                    </View>
                    <View style={styles.headerTextContainerStyle}>
                        <Text style={styles.headerTextStyle}>{currentDayOfWeek()}'s Shop</Text>
                    </View>
                </View>
                <KeyboardAvoidingView style={styles.loginContainer}>
                    <TextInput style={{...appStyles.textField, ...styles.textField}}
                               placeholder="email"
                               autoCapitalize="none"
                               autoFocus={true}
                               onChangeText={(username) => this.setState({username})} />
                    <TextInput style={{...appStyles.textField, ...styles.textField}}
                               placeholder="password"
                               autoCapitalize="none"
                               secureTextEntry={true}
                               onSubmitEditing={() => this.doLogin()}
                               onChangeText={(password) => this.setState({password})} />
                    {!!this.state.message && (
                        <Text style={{...appStyles.inlineErrorMessage, ...styles.inlineErrorMessage}}>
                            {this.state.message}
                        </Text>
                    )}
                    {this.state.isLoginInProgress && <ActivityIndicator style={styles.activityIndicator}/>}
                    <Button style={styles.button} onPress={() => this.onButtonTouch()}>
                        Login
                    </Button>
                </KeyboardAvoidingView>
            </BrandSafeAreaView>
        )
    };
};

export default Login;
