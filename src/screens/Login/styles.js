import {StyleSheet, Dimensions} from "react-native";
import { dimensions, padding, textAlign, fonts } from "app/src/styles/AppStyle";
import {AppStyles} from "app/src/styles/AppStyle"

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: padding.big
    },
    headerImageStyle: {
        flex: 2
    },
    imageFit: {
        flex: 1,
        resizeMode: "contain"
    },
    headerTextContainerStyle: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1
    },
    headerTextStyle: {
        textAlign: textAlign.center,
        fontFamily: "vincHand",
        fontSize: fonts.large
    },
    headerContainer: {
        flex: 2,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: padding.medium,
        marginRight: padding.medium,
        marginLeft: padding.medium,
        marginTop: dimensions.fullHeight * 0.1
    },
    loginContainer: {
        flex: 3,
        justifyContent: "flex-start",
        alignItems: "center",
        paddingTop: padding.medium,
        marginBottom: padding.medium,
        marginRight: padding.medium,
        marginLeft: padding.medium,
        marginTop: padding.medium
    },
    button: {
        marginTop: padding.medium,
    },
    textField: {
        marginTop: padding.small,
    },
    inlineErrorMessage: {
        padding: padding.medium
    },
    activityIndicator: {
        padding: padding.medium
    }
});

//export const styles = StyleSheet.flatten([AppStyles, localStyles]);
