// ProductList.js

import React, {Component} from "react";
import {View, BackHandler, Platform, BackAndroid} from "react-native";
import {appStyles, padding} from "app/src/styles/AppStyle";
import {styles} from "./styles";
import ProductView from "app/src/components/ProductView";
import {pikachuRandomImage} from "app/src/utility/Utility"
import BrandSafeAreaView from "app/src/components/common/BrandSafeAreaView"
import {List, FlatList} from "react-native-gesture-handler";

var loremIpsum = require("lorem-ipsum-react-native");

export class ProductList extends Component {
    static navigationOptions = {
        title: "Products",
        headerLeft: null, /// we don't want to got back to login
    };

    componentDidMount(): void {
        BackHandler.addEventListener("hardawareBackPress", this.onBackPress);
    };

    componentWillUnmount(): void {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    };

    onBackPress = () => {
        if (Platform.OS === "android") {
            BackAndroid.exitApp()
        }
    };

    transfer = ({icon, name}) => {
        console.log("transferring");
        this.props.navigation.navigate("ProductScreen", {
            icon: icon,
            name: name,
            sentences: loremIpsum({count: 25, units: "sentences"})
        });
    };

    constructor(props) {
        super(props);

        this.state = {
            data: [
                {icon: require("../../assets/list/1.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/2.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/3.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/4.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/5.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/6.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/7.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/8.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/9.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/10.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/11.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/1.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/2.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/3.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/4.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/5.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/6.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/7.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/8.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/9.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/10.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/11.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/1.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/2.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/3.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/4.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/5.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/6.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/7.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/8.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/9.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/10.jpg"), name: loremIpsum({count: 2, units: "words"}) },
                {icon: require("../../assets/list/11.jpg"), name: loremIpsum({count: 2, units: "words"}) },
            ]
        }
    }

    render() {
        return (
            <BrandSafeAreaView>
                <View style={styles.container}>
                    <FlatList style={styles.flatList}
                        data={this.state.data}
                        renderItem={({ item }) => (
                            <ProductView productIcon={item.icon} productName={item.name} onPress={this.transfer}/>
                        )}
                    />
                </View>
            </BrandSafeAreaView>
        )
    }
}

export default ProductList


