import {StyleSheet} from "react-native"
import {dimensions, padding, fonts, colors} from "app/src/styles/AppStyle"

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: dimensions.fullWidth,
        // marginTop: padding.small
    },

    flatList: {
        marginTop: 0.0,
        marginBottom: padding.medium,
    },

    headerText: {
        fontFamily: "vincHand",
        fontSize: fonts.large,
        marginTop: padding.big,
        marginBottom: padding.medium,
        alignSelf: "center",
    },
});
