
export function currentDayOfWeek() {
    var day = new Date();
    var dayWeekNumber = day.getDay();
    var dayOfWeek =  isNaN(dayWeekNumber) ? null : ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][dayWeekNumber];

    return dayOfWeek
}

export default function pikachuRandomImage() {
    const min = 1;
    const max = 19;
    const rand = min + Math.random() * (max - min);

    return "app/src/assets/list/1.jpg"
}
