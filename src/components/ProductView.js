import {styles} from "./styles";
import React, { Component } from "react";
import {Image, View, Text, TouchableHighlight} from "react-native";

export class ProductView extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const name = this.props.productName;
        const icon = this.props.productIcon;

        return(
            <TouchableHighlight key={name} onPress={() => this.props.onPress({icon, name})}>
                <View style={styles.content}>
                    <Image source={this.props.productIcon} style={styles.image} />
                    <Text style={styles.text}>{this.props.productName}</Text>
                </View>
            </TouchableHighlight>
        )
    }
}

export default ProductView;
