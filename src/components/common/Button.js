import React, {Component} from "react"
import {Text, TouchableHighlight} from "react-native"
import {appStyles, colors} from "app/src/styles/AppStyle"

class Button extends Component {
    render() {
        const {children, style, onPress} = this.props;
        return (
            <TouchableHighlight onPress={() => onPress()}
                                activeOpacity={0.6}
                                underlayColor={colors.secondary_bright_blue}
                                style={{...appStyles.buttonStyle, ...style}}>
                <Text style={appStyles.buttonContentStyle}>{children}</Text>
            </TouchableHighlight>
        );
    }
}

export default Button;
