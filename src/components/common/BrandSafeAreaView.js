import React, {Component} from "react"
import {SafeAreaView} from "react-native"
import {appStyles} from "app/src/styles/AppStyle"

class BrandSafeAreaView extends Component {
    render() {
        const {children} = this.props;
        return (
            <SafeAreaView style={appStyles.safeArea}>
                {children}
            </SafeAreaView>
        );
    }
}

export default BrandSafeAreaView;
