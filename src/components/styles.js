import {StyleSheet} from "react-native";
import { padding, fonts, colors } from "app/src/styles/AppStyle";

export const styles = StyleSheet.create({
    content: {
        flexDirection: "row",
        alignItems: "center",
        borderColor: colors.secondary_light_gray,
        borderWidth: 0.5
    },

    text: {
        fontFamily: "vincHand",
        fontSize: fonts.medium,
        marginLeft: padding.small,
        alignSelf: "stretch",
        marginTop: padding.small,
        marginBottom: padding.small
    },

    image: {
        marginLeft: padding.small,
        width: 25,
        height: 25,
        marginTop: padding.small,
        marginBottom: padding.small
    }
});
