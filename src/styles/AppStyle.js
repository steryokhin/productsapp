import {StyleSheet, Dimensions} from "react-native";

export const dimensions = {
    fullHeight: Dimensions.get("window").height,
    fullWidth: Dimensions.get("window").width
};

export const colors = {
    primary_blue: "#76cdd8",
    primary_lime_green: "#CEDB56",
    primary_graphite: "#222222",
    primary_white: "#FFFFFF",
    secondary_coral: "#D35D47",
    secondary_bright_blue: "#008ACE",
    secondary_sharp_blue: "#39C2D7",
    secondary_dark_blue: "#263852",
    secondary_light_gray: "#CCCCCC",
    secondary_dark_gray: "#464547"
};

export const padding = {
    small: 10,
    medium: 20,
    large: 30,
    big: 40
};

export const fonts = {
    small: 12,
    medium: 18,
    large: 28,
    primary: "Cochin"
};

export const textAlign = {
    center: "center",
    auto: "auto",
    left: "left",
    right: "right",
    justify: "justify"
};

/// Common application styles
export const appStyles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: colors.primary_blue
    },

    textField: {
        width: dimensions.fullWidth * 0.66,
        height: 30,
        borderWidth: 0.5,
        borderColor: colors.secondary_dark_gray,
        backgroundColor: colors.primary_white,
        paddingLeft: padding.small,
        paddingRight: padding.small
    },

    inlineErrorMessage: {
        color: "red"
    },

    buttonStyle: {
        padding: 10,
        borderWidth: 0.5,
        backgroundColor: colors.secondary_bright_blue,
        borderColor: colors.secondary_light_gray,
        borderRadius: 5
    },

    /// internal part of the button
    buttonContentStyle: {
        color: colors.primary_graphite
    }
});
