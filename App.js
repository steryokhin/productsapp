/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

// App.js

import React from "react";
import {Component} from "react";
import {createAppContainer} from "react-navigation";
import {createStackNavigator} from "react-navigation";
import Login from "app/src/screens/Login/Login";
import ProductList from "app/src/screens/ProductList/ProductList";
import Product from "app/src/screens/Product/Product";

const AppNavigator = createStackNavigator({
    LoginScreen: {screen: Login},
    ProductListScreen: {screen: ProductList},
    ProductScreen: {screen: Product}
}, {headerMode: "screen"});

const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component {
    render() {
        return (
            <AppContainer />
        );
    }
}
